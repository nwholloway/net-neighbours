NET Neighbours
==============

Utility to show devices active on your network.

The script uses non-promiscuous sniffing for ARP requests and IPv6 Neighbour Solicitation
packets to identify active devices.

The packet capture is performed by [`dumpcap`](https://www.wireshark.org/docs/man-pages/dumpcap.html)
from [_Wireshark_](https://www.wireshark.org/) to perform packet capture.  This means that
if your _Wireshark_ installation allows members of the "wireshark" group to run `dumpcap`,
this script can be run without `sudo`.

Source Documents
----------------

The [PCAP Next Generation (pcapng) Capture File Format](https://pcapng.github.io/pcapng/)
was invaluable to be able to decode the data received from `dumpcap`.
